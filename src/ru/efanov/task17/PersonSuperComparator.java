package ru.efanov.task17;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        String x1 = o1.getName();
        String x2 = o2.getName();
        int sComp = x1.compareTo(x2);

        if (sComp != 0) {
            return sComp;
        } else {
            Integer x3 = o1.getAge();
            Integer x4 = o2.getAge();
            return x3.compareTo(x4);
        }
    }
}
