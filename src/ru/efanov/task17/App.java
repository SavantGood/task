package ru.efanov.task17;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class App {
    public static void main(String[] args) {
        List<Person> person = new ArrayList<>();
        person.add(new Person("Миша", 24));
        person.add(new Person("Маша", 24));
        person.add(new Person("Дима", 24));
        person.add(new Person("Миша", 28));
        person.add(new Person("Дима", 23));
        person.add(new Person("Никита", 60));
        person.add(new Person("Аркадий", 75));

        Collections.sort(person, new PersonSuperComparator());

        for (Person Person : person) {
            System.out.println(Person.getName() + " " + Person.getAge());

        }
    }
}
