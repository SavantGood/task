package ru.efanov.task5;

import org.apache.log4j.Logger;
import java.util.Scanner;

public class App {

    private static final Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        logger.info("Начало работы программы");
        System.out.println("Внесите денежные средства");
        Scanner scanner = new Scanner(System.in);
        int money = scanner.nextInt();
        System.out.println("Вы внесли: " + money + " руб.");
        Drink[] dr = new Drink[]{Drink.MILK, Drink.COFFEE, Drink.SPRITE, Drink.TEA};
        VendingMachine vm = new VendingMachine(dr);
        vm.seleсtDrinks();
        vm.setBalance(money);
        logger.info("Вызов метода выбор напитков из класса App");
        vm.chooseDrink();

    }
}
