package ru.efanov.task5;

public enum Drink {
        TEA("Чай", 50), COFFEE("Кофе", 100), SPRITE("Спрайт", 75), MILK("Молоко", 50);

    private String Drink;
    private int price;

    Drink(String drink, int price) {
        Drink = drink;
        this.price = price;
    }

    public String getDrink() {
        return Drink;
    }

    public void setDrink(String drink) {
        Drink = drink;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return Drink + ", \tЦена: " + price + " рублей";
    }
}

