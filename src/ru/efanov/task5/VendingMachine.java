package ru.efanov.task5;

import java.util.Scanner;
import org.apache.log4j.Logger;

public class VendingMachine {
    private static final Logger logger = Logger.getLogger(VendingMachine.class.getName());
    private Drink dr[];
    private int balance;

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public VendingMachine(Drink[] dr) {
        this.dr = dr;
    }

    public void seleсtDrinks(){
        logger.info("Начало использования метода выбора напитков");
        for (int i = 0; i < dr.length; i++) {
            System.out.println(dr[i] + "\tЧто бы выбрать напиток, нажмите клваишу: " + i);
            }
    }

    public void chooseDrink () {
        System.out.println("Выберите напиток");
        Scanner scanner = new Scanner(System.in);
        int key = scanner.nextInt();
        logger.warn("Ввод с клавиатуры (Выбор напитка)");
        if ((key > dr.length - 1) || key < 0) {
            System.out.println("Такого напитка нет.");
        } else {
            Drink drink = dr[key];

            int delta;
            do {
                delta = drink.getPrice() - balance;
                String youDrink = "Ваш напиток " + drink.getDrink() + ".";
                if (delta == 0) {
                    System.out.println(youDrink);
                } else if (delta < 0) {
                    System.out.println(youDrink + " Получите сдачу " + Math.abs(delta) + " руб.");
                } else {
                    logger.error("Внесено недостаточное количество средств");
                    System.out.println("Недостаточно средств. Внесите " + Math.abs(delta) + " руб.");

                    int i = scanner.nextInt();
                    balance = balance + i;
                }
             } while (delta > 0);
            logger.info("Программа закончена");
        }
        scanner.close();
    }
}

