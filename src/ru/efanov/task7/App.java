package ru.efanov.task7;

public class App {
       public static void main(String[] args) {
        Goose goose = new Goose();
        Rat rat = new Rat();
        Misha misha = new Misha();
        Dima dima = new Dima();

        goose.getName();
        goose.swim();
        goose.run();
        goose.fly();
           System.out.println();

        rat.getName();
        rat.run();
        rat.swim();
           System.out.println();

        misha.name();
        misha.swim();
        misha.run();
           System.out.println();

        dima.name();
        dima.swim();
        dima.run();
        }
}
