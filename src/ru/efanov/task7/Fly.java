package ru.efanov.task7;

public interface Fly {
    default void fly() {
        System.out.println("Я умею летать");
    }
}
