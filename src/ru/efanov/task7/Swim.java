package ru.efanov.task7;

public interface Swim {
    default void swim () {
        System.out.println("Я умею плавать");
    }
}
