package ru.efanov.task18;

import java.util.ArrayList;
import java.util.List;

public class App implements Basket {
    private String name;
    List<String> list = new ArrayList<>();
    List<Integer> list1 = new ArrayList<>();
    int i = 0;
    int k = 0;

    public static void main(String[] args) {
        App app = new App();
        app.addProduct("Кукуруза", 2);
        app.addProduct("Каша", 3);
        app.addProduct("Молоко", 1);
        app.addProduct("Хлеб", 5);

        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.removeProduct("Кукуруза"); // Удаляем продукт
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.updateProductQuantity("Каша", 150); // Изменяем количество определенного продукта
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.getProductQuantity("Молоко"); //Выводим количество определенного продукта

        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.clear(); //Удаляем все из корзины
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

    }

    @Override
    public void addProduct(String product, int quantity) {
        list1.add(quantity);
        list.add(product);
    }

    @Override
    public void removeProduct(String product) {
        list.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
            for (String l : list) {
                if (l.equals(product)) {
                    list1.set(i, quantity);
                }
                i++;
        }
    }

    @Override
    public void clear() {
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size();) {
                list.remove(i);
            }
        }
        System.out.println("Удалено.");
    }

    @Override
    public void getProducts() {
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i) + " - " + list1.get(i) + " шт.");
            }
        } else {
            System.out.println("Корзина пуста.");
        }
    }

    @Override
    public void getProductQuantity(String product) {
        for (String l : list) {
            if (l.equals(product)) {
                System.out.println(list.get(k) + " - " + list1.get(k) + " шт.");
            }
            ++k;
        }
    }
}
