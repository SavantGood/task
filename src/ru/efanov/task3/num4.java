package ru.efanov.task3;

public class num4 {
    public static void main(String[] args) {
        System.out.println("Арифмитическая прогрессия:");
        System.out.print("{ ");
        for (int i = 1; i <= 19; i = i + 2) {
            System.out.print(i + "; ");
        }
        System.out.println("}");

        System.out.println();

        System.out.println("Геометрическая прогрессия:");
        System.out.print("{ ");
        for (int i = 1; i <= 1024; i = i * 2) {
            System.out.print(i + "; ");
        }
        System.out.print("}");
    }
}
