package ru.efanov.task3;
import java.util.Scanner;

public class num2 {
    public static void main(String[] args) {
        System.out.println("Введите число, я дам ему характеристику");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if (a < 0) {
            System.out.print(a + "  - Число отрицательное");
        }
        if (a > 0) {
            System.out.print(a + " - Число положительное");
        }
        if (a % 2 == 0 && a != 0) {
            System.out.print(" и четное.");
        }
        if (a % 2 != 0) {
            System.out.print(" и нечетное.");
        }
        if (a == 0) {
            System.out.print(a + " - Нулевое число");
        }
        scanner.close();
    }
}
