package ru.efanov.task3;
import java.util.Scanner;
import java.util.Arrays;

public class num5 {
    public static void main(String[] args) {
        System.out.println("Введите слово: ");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next();
        String reverse = new StringBuffer(word).reverse().toString();
        System.out.println("Вы ввели: " + word);
        System.out.println("Перевернутое: " + reverse);
        scanner.close();
    }
}
