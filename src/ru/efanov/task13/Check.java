package ru.efanov.task13;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Check {
    private static final String fileName = "products.txt";
    static float l4 = 0; // Итоговая сумма за товар
    static float l5 = 0; // Итоговая сумма


    public static void main(String[] args) {

        try (PrintStream out = new PrintStream(new FileOutputStream("Check.txt"))
        ) {
            System.setOut(out);
            System.out.println("Наименование        Цена   Кол-во    Стоимость");
            System.out.println("===============================================");
            int i;
            for (i = 0; i < 24; i = i++ + 3){
                String l1 = Files.readAllLines(Paths.get(fileName)).get(i);
                String l2 = Files.readAllLines(Paths.get(fileName)).get(i+1);
                String l3 = Files.readAllLines(Paths.get(fileName)).get(i+2);
                System.out.printf("%-15s", l1);
                System.out.printf("%10s", l3);
                System.out.printf("%2s %5s", "x", l2);
                float iP = Float.parseFloat(l2);
                float iM = Float.parseFloat(l3);
                l4 = iP * iM;
                l5 = l4 + l5;
                System.out.printf("%5s %2.2f%n", "=", l4);
                }
            System.out.println("===============================================");
            System.out.printf("%6s: %38.2f", "Итого", l5);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
