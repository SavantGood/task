package ru.efanov.task19;

import java.util.*;

public class Filter {
    public static Set<String> set = new HashSet<>();

    public static void main(String[] args) {
        Filter.add("foo");
        Filter.add("buzz");
        Filter.add("bar");
        Filter.add("fork");
        Filter.add("bort");
        Filter.add("spoon");
        Filter.add("!");
        Filter.add("dude");

        removeEvenLength1(set);

        System.out.println(set);

    }

    private static  Set<String> removeEvenLength1(Set<String> set) {
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            if (str.length() % 2 == 0) {
                iterator.remove();
            }
        }
        return set;
    }


    private static void add(String i) {
        set.add(i);
    }
}
