package ru.efanov.task15;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

public class Inthernet {
    public static void main(String[] args) {
       try {
           URL url = new URL("https://official-joke-api.appspot.com/jokes/random");
           try (BufferedReader readerURL = new BufferedReader(new InputStreamReader(url.openStream()))) {

               ObjectMapper objectMapper = new ObjectMapper();
               Conversation conversation = objectMapper.readValue(readerURL, Conversation.class);
               objectMapper.writeValue(System.out, conversation);
           }


       }
       catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

