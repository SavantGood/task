package ru.efanov.task20;

import ru.efanov.task18.Basket;
import java.util.*;

public class App implements Basket {
    private static Map<String, Integer> map = new HashMap<>();

    public static void main(String[] args) {
        ru.efanov.task20.App app = new App();
        app.addProduct("Кукуруза", 2);
        app.addProduct("Каша", 3);
        app.addProduct("Молоко", 1);
        app.addProduct("Хлеб", 5);


        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.removeProduct("Кукуруза"); // Удаляем продукт
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.updateProductQuantity("Каша", 150); // Изменяем количество определенного продукта
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.getProductQuantity("Молоко"); //Выводим количество определенного продукта
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");

        app.clear(); //Удаляем все из корзины
        app.getProducts(); //Выводим список продуктов
        System.out.println("------------------------------------");
        System.out.println("------------------------------------");
    }

    @Override
    public void addProduct(String product, int quantity) {
        map.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        map.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals(product)) {
                map.put(product, quantity);
            }
        }
    }

    @Override
    public void clear() {
        Iterator<Map.Entry<String, Integer>> itr = map.entrySet().iterator();
            while (itr.hasNext()) {
                itr.next();
                itr.remove();
            }
        System.out.println("Удалено.");
    }

    @Override
    public void getProducts() {

        if (!map.isEmpty()) {
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue() + " шт.");
            }
        } else {
            System.out.println("Корзина пуста.");
        }
    }

    @Override
    public void getProductQuantity(String product) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getKey().equals(product)) {
                System.out.println(entry.getValue());
            }
        }
    }
}
