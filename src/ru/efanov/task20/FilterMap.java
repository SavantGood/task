package ru.efanov.task20;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FilterMap {
    private static Map<String, String> map = new HashMap<>();
    private static Set<String> set = new HashSet<>();

    public static void main(String[] args) {
        add("Вася", "Иванов");
        add("Женя", "Иванов1");
        add("Петр", "Петров");
        add("Виктор", "Сидоров");
        add("Сергей", "Савельев");
        add("Вадим", "Викторов");


        FilterMap fp = new FilterMap();

        System.out.println(fp.isUnique(map));

    }

    private static void add(String x, String y) {
        map.put(x, y);
    }

    public boolean isUnique(Map<String, String> map) {

        set.addAll(map.values());

        if (set.size() != map.values().size()) {
            return false;
        }
        return true;
    }
}
