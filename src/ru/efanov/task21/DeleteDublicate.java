package ru.efanov.task21;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DeleteDublicate {
    public static void main(String[] args) {
        Map<String, Person> map = createMap();
        removeTheDuplicates(map);
        for (Map.Entry<String, Person> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

    public static Map<String, Person> createMap()
    {
        Map<String, Person> book = new HashMap<>();
        Person person1 = new Person(29,"Петрова","жен");
        Person person2 = new Person(34, "Сидорова", "жен");
        Person person3 = new Person(34, "Тихонова", "жен");
        Person person4 = new Person(35, "Петров", "муж");
        book.put("Key1", person1);
        book.put("Key2", person1);
        book.put("Key3", person2);
        book.put("Key4", person3);
        book.put("Key5", person4);
        book.put("Key6", person4);
        return book;
    }

    public static void removeTheDuplicates(Map<String, Person> map){
        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> entry : copy.entrySet()) {
            int frequency = Collections.frequency(copy.values(), entry.getValue());
            if (frequency > 1) {
                removeItemFromMapByValue(map, entry.getValue());
            }
        }
    }



    public static void removeItemFromMapByValue(Map<String, Person> map, Person value)
    {
        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}


