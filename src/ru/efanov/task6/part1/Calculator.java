package ru.efanov.task6.part1;

public class Calculator {
    private static double c;

    public static int sum (int a, int b) {
        return a + b;
    }
    public static double sum (double a, double b) {
        return a + b;
    }

    public static int vich (int a, int b) {
        return a - b;
    }
    public static double vich (double a, double b) {
        return a - b;
    }

    public static int ymnoj (int a, int b) {
        return a * b;
    }
    public static double ymnoj (double a, double b) {
        return a * b;
    }

    public static int del (int a, int b) {
        return a / b;
    }
    public static double del (double a, double b) {
        return a / b;
    }

    public static double proc (double a, double b) {
        return a * (b / 100);
    }


    public static void main(String[] args) {
        System.out.println("Сложение: " + Calculator.sum(5.4, 5));

        System.out.println("Вычитание: " + Calculator.vich(5.4, 5));

        System.out.println("Умножение: " + Calculator.ymnoj(5.4, 5));

        System.out.println("Деление: " + Calculator.del(5.4, 5.4));

        System.out.println("Процент от числа: " + Calculator.proc(100.1, 1));



    }
}
