package ru.efanov.task6.part3;

public class Converter {
    public static Act convert (Contract contract) {
        return new Act(contract.getNumber(), contract.getDate(), contract.getBase());
    }
}
