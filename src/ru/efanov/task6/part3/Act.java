package ru.efanov.task6.part3;

import java.time.LocalDate;
import java.util.Arrays;

public class Act {
    private int number;
    private LocalDate date;
    private String base[];

    public Act(int number, LocalDate date, String[] base) {
        this.number = Contract.getNumber();
        this.date = Contract.getDate();
        this.base =  Contract.getBase();
    }

    public static void main(String[] args) {
        Contract.getNumber();
        Contract.getDate();
        Contract.getBase();
    }

    @Override
    public String toString() {
        return "Акт в соответствии с договором # " + number +
                "\nНомер: " + number +
                "\nДата: " + date +
                "\nСписок брендов: " + Arrays.toString(base);
    }
}
