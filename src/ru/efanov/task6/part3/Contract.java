package ru.efanov.task6.part3;

import java.time.LocalDate;

public class Contract {
    private static int number;
    private static LocalDate date;
    private static String base[];

    public static void main(String[] args) {
        Contract c1 = new Contract(111, LocalDate.now(), new String[] {"Apple", "Samsung", "Honor"});
        System.out.println(Converter.convert(c1));
    }

    public Contract(int number, LocalDate date, String[] base) {
        this.number = number;
        this.date = date;
        this.base = base;
    }

    public static int getNumber() {
        return number;
    }

    public static LocalDate getDate() {
        return date;
    }

    public static String[] getBase() {
        return base;
    }
}
