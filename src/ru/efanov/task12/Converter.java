package ru.efanov.task12;

import java.io.*;
import java.util.Scanner;


public class Converter {

    private static String  s = "Привет, Мир!";
    static String fileName = "test.txt";

    public static void main(String[] args) throws IOException {
        File firstFile = new File(fileName);

        try (Writer firstWriter = new OutputStreamWriter(new FileOutputStream(firstFile))) {
            firstWriter.write(s);

        }
        convert();
        }

    private static void convert() throws IOException {
        File secondFile = new File ("newTest.txt");
        System.out.println("Введите способ кодировки: ");
        Scanner sc = new Scanner(System.in);
        String charsetName = sc.nextLine();

        try (Writer secondWriter = new OutputStreamWriter(new FileOutputStream(secondFile), charsetName)){
            FileReader fr = new FileReader(fileName);
            int a;
            while ((a = fr.read()) != -1) {
                secondWriter.write(a);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("Не существующая кадировка: " + e.getMessage());
        }
    }
}
