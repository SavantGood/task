package ru.efanov.task10;


import java.io.File;


public class RecursiveWithSnow {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Savant\\IdeaProjects\\task\\dir");

        recursiveSnow(file);
    }
    private static void recursiveSnow(File file) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                if (file1.isFile()) {
                    file1.renameTo(new File("qwerty"));
                } else {
                    System.out.println("Имя директории: " + file.getName());
                    recursiveSnow(file1);
                }
            }
    }
}
