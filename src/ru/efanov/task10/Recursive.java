package ru.efanov.task10;

import java.io.File;

public class Recursive {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Savant\\IdeaProjects\\task\\src");


        recursiveTest(file);
    }
    private static void recursiveTest(File file) {
        File[] files = file.listFiles();
        for (File file1 : files) {
            if (file1.isFile()) {
                System.out.println("Имя файла: " + file1.getName());
            } else {
                System.out.println(" ");
                System.out.println("Имя директории: " + file1.getName());
                System.out.println(" ");
                recursiveTest(file1);
            }
        }
    }
}
