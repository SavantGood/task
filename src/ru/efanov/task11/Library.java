package ru.efanov.task11;

import java.io.Serializable;

public class Library implements Serializable {
    private String bookName;
    private String author;
    private int madeYear;

    public Library() {
    }

    public String getBookName() {
        return this.bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getMadeYear() {
        return this.madeYear;
    }

    public void setMadeYear(int madeYear) {
        this.madeYear = madeYear;
    }

    public String toString() {
        return "Library{bookName='" + this.bookName + '\'' + ", author='" + this.author + '\'' + ", madeYear=" + this.madeYear + '}';
    }
}

