package ru.efanov.task11;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

public class App {
    private static final String fileName = "Library.bin";
    public static Library[] books;

    private static void addBook(Object[] books) {
        Library bk = new Library();

        Scanner sc = new Scanner(System.in);

        System.out.println("Добавьте книгу. ");
        System.out.println("Введите название книги: ");
        String bkName = sc.nextLine();

        System.out.println("Введите автора книги: ");
        String bkAuthor = sc.nextLine();

        System.out.println("Введите год издания книги: ");
        int bkMadeYear = sc.nextInt();

        bk.setBookName(bkName);
        bk.setAuthor(bkAuthor);
        bk.setMadeYear(bkMadeYear);

        App.books = new Library[]{bk};


    }

    public static void main(String[] args) throws Exception {
        addBook(books);

        write(books);



        System.out.println("До записи: " + Arrays.toString(books));
        System.out.println("Список книг в библиотеке: ");
        System.out.println(Arrays.toString(load()));


    }

    private static void write(Library[] books) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName, true))) {

            oos.writeObject(books);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static Library[] load() throws IOException, ClassNotFoundException {
        try (InputStream is = new FileInputStream(fileName)) {
            byte[] buf = new byte[100000];
            if (is.read(buf) != -1) {
                String s = new String(buf, StandardCharsets.UTF_8);
                System.out.println(s);
            }
        }
        return new Library[0];
    }

}

