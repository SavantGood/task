package ru.efanov.task9;

public enum Food {
    KASHA("Каша"), ICE_CREAM ("Мороженое"), APPLE("Яблоко"), MILK("Молоко");

    private String title;

    @Override
    public String toString() {
        return title + "\t- ";
    }

    Food(String title) {
        this.title = title;
    }
}
