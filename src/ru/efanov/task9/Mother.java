package ru.efanov.task9;

public class Mother {

    public static void main(String[] args) throws NotLikeException{
        Food[] foods = new Food [] {Food.ICE_CREAM, Food.APPLE, Food.KASHA,  Food.MILK};
        Child.selectFood(foods);

        try {
            Child.chooseFood(foods);
        } catch (NotLikeException e) {
            System.out.println(e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Такой еды нету");
        }

    }
}
