package ru.efanov.task16.part2;

import ru.efanov.task16.part1.MassiveToLeft;

public class MassiveReverse {
    private long[] a;
    private int nElems;

    public MassiveReverse(int max)
    {
        a = new long[max];
        nElems = 0;
    }


    public void insert(long value)
    {
        a[nElems] = value;
        nElems++;
    }


    public void display()
    {
        for (int j = 0; j < nElems; j++)
            System.out.print(a[j] + " ");
        System.out.println("");
    }


    public void sortReverse() {
        int out, in;

        for (out = nElems - 1; out > 1; out--){
            for (in = 0; in < out; in++){
                swap(in, in + 1);
            }
        }
        swap(0, 1);
    }



    private void swap(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }

}

class BubbleSortApp {
    public static void main(String[] args) {
        int maxSize = 100;
        MassiveReverse arr;
        arr = new MassiveReverse(maxSize);

        arr.insert(8);
        arr.insert(2);
        arr.insert(3);
        arr.insert(5);
        arr.insert(6);


        arr.display();

        arr.sortReverse();

        arr.display();
    }
}
