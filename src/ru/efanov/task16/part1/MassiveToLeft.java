package ru.efanov.task16.part1;

public class MassiveToLeft {
    private long[] a;
    private int nElems;

    public MassiveToLeft(int max)
    {
        a = new long[max];
        nElems = 0;
    }


    public void insert(long value)
    {
        a[nElems] = value;
        nElems++;
    }


    public void display()
    {
        for (int j = 0; j < nElems; j++)
            System.out.print(a[j] + " ");
        System.out.println("");
    }


    public void sort1() {
        int in;

            for (in = 0; in < nElems; in++){
                swap(in, in + 1);
            }
    }



    private void swap(int one, int two) {
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }

}

class BubbleSortApp2 {
    public static void main(String[] args) {
        int maxSize = 100;
        MassiveToLeft arr;
        arr = new MassiveToLeft(maxSize);

        arr.insert(8);
        arr.insert(2);
        arr.insert(3);
        arr.insert(5);
        arr.insert(8);


        arr.display();

        arr.sort1();

        arr.display();
    }
}
