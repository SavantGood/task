package ru.efanov.task4;

import java.time.LocalDate;

public class Administration extends Person {

    private int salary;

    public Administration(String name, String lastName, LocalDate birthday, int apartamentNumber, int salary) {
        super(name, lastName, birthday, apartamentNumber);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
