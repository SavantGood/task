package ru.efanov.task4;

import java.time.LocalDate;

public class Residents extends Person {
    private int pasportNumber;

    public Residents(String name, String lastName, LocalDate birthday, int apartamentNumber) {
        super(name, lastName, birthday, apartamentNumber);
        this.pasportNumber = pasportNumber;
    }

    public int getPasportNumber() {
        return pasportNumber;
    }

    public void setPasportNumber(int pasportNumber) {
        this.pasportNumber = pasportNumber;
    }
}
