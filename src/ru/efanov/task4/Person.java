package ru.efanov.task4;

import java.time.LocalDate;

public class Person {
    private String name;
    private String lastName;
    private LocalDate birthday;
    private int apartamentNumber;

    public Person(String name, String lastName, LocalDate birthday, int apartamentNumber) {
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.apartamentNumber = apartamentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public int getApartamentNumber() {
        return apartamentNumber;
    }

    public void setApartamentNumber(int apartamentNumber) {
        this.apartamentNumber = apartamentNumber;
    }

    @Override
    public String toString() {
        return "\n" + "Person{" +
                "name = '" + name + '\'' +
                ", lastName = '" + lastName + '\'' +
                ", birthday = " + birthday +
                ", apartamentNumber = " + apartamentNumber +
                '}';
    }
}

