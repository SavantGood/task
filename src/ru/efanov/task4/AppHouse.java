package ru.efanov.task4;

import java.time.LocalDate;
import java.util.Arrays;

public class AppHouse {
    public static void main(String[] args) {
        Residents misha = new Residents("Михаил", "Ефанов", LocalDate.of(1994, 8, 26), 132);
        Residents nik = new Residents("Никита", "Дроздов", LocalDate.of(1993, 10, 8), 128);
        Residents dima = new Residents("Дима", "Жижикин", LocalDate.of(1993, 11, 23), 1);
        Residents artem = new Residents("Артем", "Анич", LocalDate.of(1994, 4, 4), 5);

        misha.setPasportNumber(123);
        nik.setPasportNumber(321);
        dima.setPasportNumber(555);
        artem.setPasportNumber(777);

        House n10 = new House(10);
        Floors f3 = new Floors(3);
        FrontDoors d3 = new FrontDoors(3);

        Floors f1 = new Floors(1);
        FrontDoors d1 = new FrontDoors(1);

        f3.setResidents(new Residents[]{misha, nik});
        d3.setFloors(new Floors[]{f3});

        f1.setResidents(new Residents[]{dima, artem});
        d1.setFloors(new Floors[]{f1});

        n10.setFrontDoors(new FrontDoors[]{d1, d3});

        System.out.println(n10);


    }
}
