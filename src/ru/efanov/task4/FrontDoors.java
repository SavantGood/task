package ru.efanov.task4;

import java.util.Arrays;

public class FrontDoors {
    private int numberDoor;
    private Floors[] floors = new Floors[25];

    public FrontDoors(int numberDoor) {
        this.numberDoor = numberDoor;
    }

    public int getNumberDoor() {
        return numberDoor;
    }

    public void setNumberDoor(int numberDoor) {
        this.numberDoor = numberDoor;
    }

    public Floors[] getFloors() {
        return floors;
    }

    public void setFloors(Floors[] floors) {
        this.floors = floors;
    }

    @Override
    public String toString() {
        return "\n" + "\n"  + "{" +
                "numberDoor = " + numberDoor +
                ", floors = " + Arrays.toString(floors) +
                '}';
    }
}
