package ru.efanov.task4;

import java.util.Arrays;

public class House {
    private int houseNumber;
    private FrontDoors[] frontDoors = new FrontDoors[4];

    public House(int houseNumber) {
        this.houseNumber = houseNumber;
    }


    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public FrontDoors[] getFrontDoors() {
        return frontDoors;
    }

    public void setFrontDoors(FrontDoors[] frontDoors) {
        this.frontDoors = frontDoors;
    }

    @Override
    public String toString() {
        return "House {" +
                "houseNumber = " + houseNumber +
                ", frontDoors = " + Arrays.toString(frontDoors) +
                '}';
    }
}
