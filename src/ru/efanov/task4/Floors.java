package ru.efanov.task4;

import java.util.Arrays;

public class Floors {
    private int numberFloor;
    private Residents[] residents = new Residents[6];

    public Floors(int numberFloor) {
        this.numberFloor = numberFloor;
    }

    public int getNumberFloor() {
        return numberFloor;
    }

    public void setNumberFloor(int numberFloor) {
        this.numberFloor = numberFloor;
    }

    public Residents[] getResidents() {
        return residents;
    }

    public void setResidents(Residents[] residents) {
        this.residents = residents;
    }

    @Override
    public String toString() {
        return numberFloor +
                ", residents = " + Arrays.toString(residents) +
                '}';
    }
}
